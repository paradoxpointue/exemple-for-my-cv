import pygame,sys,os
from pygame.locals import *
from pygame.constants import KEYDOWN, VIDEORESIZE
clock = pygame.time.Clock()
COUNT = 0
pygame.init()
screen = pygame.display.set_mode((800,600),pygame.RESIZABLE)
pygame.display.set_caption("Fractihou")
fullscreen = False
vec = pygame.math.Vector2
ACC = 0.5
WIDTH = screen.get_width()*0.7-15
HEIGHT = screen.get_height()-15

class Game():
    def __init__(self):
        super().__init__()
        self.state="Level1"

    def MainMenu(self):
        screen.fill((255,255,255))

    def Level(self):
        screen.fill((255,255,255))
        pygame.draw.rect(screen,(255,0,0),pygame.Rect(0,0,screen.get_width(),screen.get_height()))
        pygame.draw.rect(screen,(0,0,0), pygame.Rect(15,15,screen.get_width()*0.7-15,screen.get_height()-30))
        player.move()
        screen.blit(player.image, player.rect) 
    
    def Loading(self):
        if self.state=="Level1":
            self.Level()
            return

        if self.state=="Main_Menu":
            self.MainMenu()
            return

        print("error: invalid self.state")

class Player(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load("player.png")
        self.rect = self.image.get_rect()
        self.pos = vec((340, 240))
        self.vel = vec(0,0)
        self.acc = vec(0,0)
        
    def move(self):
      if abs(self.vel.x) > 0.3:
            self.running = True
      else:
            self.running = False
      pressed_keys = pygame.key.get_pressed()
      if pressed_keys[K_RIGHT]:
          self.pos.x += 5
      if pressed_keys[K_LEFT]:
          self.pos.x -= 5
      if pressed_keys[K_UP]:
          self.pos.y -= 5
      if pressed_keys[K_DOWN]:
          self.pos.y += 5
      if self.pos.x > WIDTH:
            self.pos.x = WIDTH
      if self.pos.x < 30:
            self.pos.x = 30
      if self.pos.y>HEIGHT:
          self.pos.y=HEIGHT
      if self.pos.y<45:
          self.pos.y=45
      self.rect.midbottom = self.pos
 
    def update(self):
      pass
 
    def attack(self):
      pass
player=Player()
Game=Game()
#the actual game
while True: 
    Game.Loading()
    for event in pygame.event.get():
        if event.type==pygame.QUIT:
            pygame.quit()
            sys.exit(0)
        if event.type==KEYDOWN:
            if event.key==K_F11:
                fullscreen=not fullscreen
                if fullscreen:
                    old_size=(screen.get_width(),screen.get_height())
                    os.environ['SDL_VIDEO_WINDOW-POS']="%d,%d"%(0,0)
                    screen=pygame.display.set_mode((screen.get_width(),screen.get_height()),pygame.FULLSCREEN)
                    pygame.display.update()
                else:
                    screen=pygame.display.set_mode((old_size),pygame.RESIZABLE)
        if event.type==VIDEORESIZE:
            screen=pygame.display.set_mode((event.w,event.h),pygame.RESIZABLE)
    pygame.display.update()
    clock.tick(60)